package nfc.mbds.ici;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.w3c.dom.Text;

public class CreationEtudiant extends Activity {

    private Button btnValider = null;
    private Button btnRetour = null;
    private EditText txtNumeroEtudiant = null;
    private EditText txtNom = null;
    private EditText txtPrenom = null;
    private EditText txtPassword = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);

        btnValider = (Button) findViewById(R.id.valider);
        btnRetour = (Button) findViewById(R.id.annuler);
        txtNumeroEtudiant = (EditText) findViewById(R.id.numeroEtudiant);
        txtNom = (EditText) findViewById(R.id.nomEtudiant) ;
        txtPrenom = (EditText) findViewById(R.id.prenomEtudiant);
        txtPassword = (EditText) findViewById(R.id.password);
        checkFieldsForEmptyValues();

        txtNumeroEtudiant.addTextChangedListener(mTextWatcher);
        txtNom.addTextChangedListener(mTextWatcher);
        txtPrenom.addTextChangedListener(mTextWatcher);
        txtPassword.addTextChangedListener(mTextWatcher);

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreationEtudiant.this.finish();
            }
        });

        btnValider.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent creationEtudiantTagActivite = new Intent(CreationEtudiant.this, CreationEtudiantTag.class);

                int numeroEtudiant = (int) Integer.parseInt(txtNumeroEtudiant.getText().toString());
                String nomEtudiant = txtNom.getText().toString();
                String prenomEtudiant = txtPrenom.getText().toString();
                String password = txtPassword.getText().toString();
                creationEtudiantTagActivite.putExtra("numeroEtudiant",numeroEtudiant);
                creationEtudiantTagActivite.putExtra("nomEtudiant", nomEtudiant);
                creationEtudiantTagActivite.putExtra("prenomEtudiant", prenomEtudiant);
                creationEtudiantTagActivite.putExtra("password", password);

                startActivity(creationEtudiantTagActivite);
            }
        });
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkFieldsForEmptyValues();
        }
    };

    void checkFieldsForEmptyValues(){
        if(TextUtils.isEmpty(txtNom.getText()) || TextUtils.isEmpty(txtPrenom.getText()) || TextUtils.isEmpty(txtNumeroEtudiant.getText()) || TextUtils.isEmpty(txtPassword.getText())){
            btnValider.setEnabled(false);
        } else {
            btnValider.setEnabled(true);
        }
    }
}
