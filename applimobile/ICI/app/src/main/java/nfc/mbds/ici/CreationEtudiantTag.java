package nfc.mbds.ici;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import nfc.mbds.ici.bdd.Etudiant;
import nfc.mbds.ici.bdd.EtudiantManager;
import nfc.mbds.ici.communicationApi.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreationEtudiantTag extends Activity {

    private TextView txtTagNFC = null;
    private Button btnValider = null;
    private TextView txtResultatInsert = null;
    private Button btnRetour = null;
    private Etudiant newEtudiant = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fin_inscription);

        txtTagNFC = (TextView) findViewById(R.id.editTagNFC);
        btnValider = (Button) findViewById(R.id.valider2);
        txtResultatInsert = (TextView) findViewById(R.id.txtResultatInsert);
        btnRetour = (Button)  findViewById(R.id.retour);

        Intent intent = getIntent();
        final int numero = intent.getIntExtra("numeroEtudiant",10000000);
        final String nom = intent.getStringExtra("nomEtudiant");
        final String prenom = intent.getStringExtra("prenomEtudiant");
        final String password = intent.getStringExtra("password");
        txtTagNFC.setText(getIntent().getStringExtra("idTag"));

        final EtudiantManager etudiantManager = new EtudiantManager(this);
        etudiantManager.open();

        newEtudiant = new Etudiant(numero, nom, prenom, password, null);

        if(txtResultatInsert.getText().length() >0) {
            btnValider.setEnabled(true);
        } else {
            btnValider.setEnabled(false);
        }

        if(etudiantManager.addEtudiant(newEtudiant) == -1 && txtTagNFC.getText().length() == 0 ){
            txtResultatInsert.setText("L'étudiant existe déjà, veuillez modifier les informations en appuyant sur le bouton Retour");
            txtResultatInsert.setTextColor(Color.RED);
            btnValider.setEnabled(false);
        } else if(txtTagNFC.getText().length() != 0){
            btnValider.setEnabled(true);
            btnRetour.setEnabled(false);
        }

        final Context context = this;

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etudiantManager.supEtudiant(newEtudiant);
                CreationEtudiantTag.this.finish();
            }
        });

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String tag = txtTagNFC.getText().toString();
                int numeroEtudiant = etudiantManager.getEtudiantSansTag();
                Etudiant etudiant = etudiantManager.getEtudiant(numeroEtudiant);

                if (etudiant.getNumeroEtudiant()!= 0){
                    etudiant.setTag_nfc(tag);
                    etudiantManager.modEtudiant(etudiant);


                    Etudiant newEtudiant = etudiantManager.getEtudiant(etudiant.getNumeroEtudiant());
                    etudiantManager.close();

                    Api.getInstance(context).envoieNewEtudiant(etudiant, new retrofit2.Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            if(response.errorBody() != null){

                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {

                        }
                    });

                    Intent finInscription = new Intent(CreationEtudiantTag.this, MainActivity.class);
                    startActivity(finInscription);
                    finInscription.setFlags(finInscription.FLAG_ACTIVITY_CLEAR_TOP);
                    CreationEtudiantTag.this.finish();
                } else {
                    etudiantManager.close();

                }
            }
        });

    }
}
