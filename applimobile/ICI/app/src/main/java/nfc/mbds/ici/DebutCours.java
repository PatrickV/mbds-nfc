package nfc.mbds.ici;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.CoursManager;
import nfc.mbds.ici.bdd.Enseignant;
import nfc.mbds.ici.bdd.EnseignantManager;
import nfc.mbds.ici.bdd.Presence;
import nfc.mbds.ici.bdd.PresenceManager;
import nfc.mbds.ici.communicationApi.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DebutCours extends Activity {

    private Button btnRetour = null;
    private Button btnValider = null;
    private Spinner listeCours = null;
    private EditText txtCodeProfesseur = null;
    private TextView txtInfo = null;

    List<String> cours = new ArrayList<>();
    ArrayAdapter<String> adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.debut_cours);

        final CoursManager coursManager = new CoursManager(this);
        final PresenceManager presenceManager = new PresenceManager(this);
        final EnseignantManager enseignantManager = new EnseignantManager(this);

        coursManager.open();
        enseignantManager.open();

        btnRetour = (Button) findViewById(R.id.annuler);
        txtCodeProfesseur = (EditText) findViewById(R.id.codeProfesseur);
        txtInfo = (TextView) findViewById(R.id.txtInfo);
        btnValider = (Button) findViewById(R.id.valider);
        listeCours = (Spinner) findViewById(R.id.spinner);
        checkFieldsForEmptyValues();
        txtCodeProfesseur.addTextChangedListener(mTextWatcher);

        Api.getInstance(this).getListeEnseignant(new Callback<List<Enseignant>>() {
            @Override
            public void onResponse(Call<List<Enseignant>> call, Response<List<Enseignant>> response) {
                if(response.code() == 200) {
                    for (int i = 0; i > response.body().size(); i++)
                        enseignantManager.addEnseignant(response.body().get(i));
                } else {
                    enseignantManager.addEnseignant(new Enseignant(1,"Dupont","1234"));
                    enseignantManager.addEnseignant(new Enseignant(2,"LaFleure","5678"));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                enseignantManager.addEnseignant(new Enseignant(1,"Dupont","1234"));
                enseignantManager.addEnseignant(new Enseignant(2,"LaFleure","5678"));

            }
        });

        Api.getInstance(this).getListeCours(new Callback<List<Cours>>() {
            @Override
            public void onResponse(Call<List<Cours>> call, Response<List<Cours>> response) {
                if(response.code() == 200) {
                    for (int i = 0; i > response.body().size(); i++) {
                        coursManager.addCours(response.body().get(i));
                    }
                } else {
                    Date dateDuJour = new Date();
                    SimpleDateFormat formatDateDuJour = new SimpleDateFormat("dd-MM-yyyy");
                    String dateJour = formatDateDuJour.format(dateDuJour);

                    coursManager.addCours(new Cours(1,"Maths",1, dateJour,null,"pasCommmence"));
                    coursManager.addCours(new Cours(2,"Anglais",2,dateJour,null,"pasCommmence"));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Date dateDuJour = new Date();
                SimpleDateFormat formatDateDuJour = new SimpleDateFormat("dd-MM-yyyy");
                String dateJour = formatDateDuJour.format(dateDuJour);

                //Création de cours pour le test
                coursManager.addCours(new Cours(1,"Maths",1, dateJour,null,"pasCommmence"));
                coursManager.addCours(new Cours(2,"Anglais",2,dateJour,null,"pasCommmence"));
            }
        });


        cours = coursManager.getListCoursJour();
        adapter = new ArrayAdapter<String>(DebutCours.this,android.R.layout.simple_spinner_dropdown_item, android.R.id.text1,cours);
        listeCours.setAdapter(adapter);
        listeCours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),""+parent.getItemAtPosition(position).toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebutCours.this.finish();
            }
        });

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomCoursSelectionne = listeCours.getSelectedItem().toString();
                Cours coursSelectionne = coursManager.getCoursByNom(nomCoursSelectionne);
                enseignantManager.open();

                Enseignant enseignant = enseignantManager.getEnseignant(coursSelectionne.getIdProfesseur());

                if (enseignant.getCodeEnseignant().equals(txtCodeProfesseur.getText().toString())) {
                    presenceManager.open();
                    Presence presence = new Presence(0, 0, "", "", "");
                    presenceManager.addPresence(presence);

                    Date heureDebut = new Date();
                    SimpleDateFormat formatHeureDebut = new SimpleDateFormat("HH:mm");
                    String heure = formatHeureDebut.format(heureDebut);

                    Cours debutCours = new Cours(coursSelectionne.getCodeCours(), coursSelectionne.getNomCours(), coursSelectionne.getIdProfesseur(), coursSelectionne.getDateCours(), heure, "enCours");
                    coursManager.modCours(debutCours);

                    Intent creationEtudiantActivite = new Intent(DebutCours.this, PresenceEtudiant.class);

                    startActivity(creationEtudiantActivite);
                } else {
                    txtInfo.setText("Mauvaise code");
                    txtInfo.setTextColor(Color.RED);
                }


            }
        });
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkFieldsForEmptyValues();
        }
    };

    void checkFieldsForEmptyValues(){
        if(TextUtils.isEmpty(txtCodeProfesseur.getText())){
            btnValider.setEnabled(false);
        } else {
            btnValider.setEnabled(true);
        }
    }
}
