package nfc.mbds.ici;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.CoursManager;
import nfc.mbds.ici.bdd.Presence;
import nfc.mbds.ici.bdd.PresenceManager;
import nfc.mbds.ici.communicationApi.Api;
import retrofit2.Call;
import retrofit2.Response;

public class FinCours extends Activity {
    ListView liste;
    Button btnTermine;
    List<String> cours = new ArrayList<>();
    PresenceManager presenceManager = new PresenceManager(this);
    private SimpleCursorAdapter  dataAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final CoursManager coursManager = new CoursManager(this);
       //final PresenceManager presenceManager = new PresenceManager(this);
        presenceManager.open();
        coursManager.open();
        cours = coursManager.getListCoursJour();

        setContentView(R.layout.fin_cours);

        liste = (ListView) findViewById(R.id.resultatPresence);
        btnTermine = (Button) findViewById(R.id.fermer);
        final Context context = this;
        Cours coursEnCours = coursManager.getCoursEnCours();

        int numeroCours = coursEnCours.getCodeCours();

        displayListView(numeroCours);




        btnTermine.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                Cours coursEnCours = coursManager.getCoursEnCours();

                int numeroCours = coursEnCours.getCodeCours();

                List<Object> listePresence = null;
                listePresence = presenceManager.getListePresence(numeroCours);
                Intent revenirMainActivity = new Intent(FinCours.this,MainActivity.class);
                startActivity(revenirMainActivity);
                FinCours.this.finish();

                int nbEtudiantPresent = presenceManager.getNombreEtudiantPresent(coursEnCours.getCodeCours());
                listePresence = listePresence.subList(0,nbEtudiantPresent);
                Api.getInstance(context).envoieListePresence(listePresence, new retrofit2.Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {
                        if(response.errorBody() != null){

                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        System.out.println("-----------------------ERREUR-------------------" + t);
                    }
                });


                coursEnCours.setCoursFini("termine");
                coursManager.modCours(coursEnCours);


            }
        });

    }

    private void displayListView(int numeroCours){
        Cursor cursor = presenceManager.getPresences(numeroCours);
        String[] columns = new String[]{
                "_id", "PrenomEtudiant", "NomEtudiant", "statutEtudiant"
        };

        int [] to = new int []{
                R.id.txtNumligne,
                R.id.txtPrenomEtudiant,
                R.id.txtNomEtudiant,
                R.id.txtStatutEtudiant
                };

        dataAdapter = new SimpleCursorAdapter(this, R.layout.ligne_resultat_presence, cursor,columns,to,0);

        liste.setAdapter(dataAdapter);
    }

}
