package nfc.mbds.ici;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.CoursManager;
import nfc.mbds.ici.bdd.Etudiant;
import nfc.mbds.ici.bdd.EtudiantManager;
import nfc.mbds.ici.bdd.Presence;
import nfc.mbds.ici.bdd.PresenceManager;

public class  LectureTag extends Activity {

    private String uid = null;
    private Etudiant etudiant = null;
    private Cours cours = null;
    private Presence presence = null;
    private String statutEtudiant = null;


    protected void onNewIntent() {
        Tag tag = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
        this.uid = bin2hex(tag.getId());
    }

    private String bin2hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onNewIntent();
        System.out.println(uid);

        final EtudiantManager etudiantManager = new EtudiantManager(this);
        final PresenceManager presenceManager = new PresenceManager(this);
        final CoursManager coursManager = new CoursManager(this);
        etudiantManager.open();
        presenceManager.open();
        coursManager.open();

        etudiant =  etudiantManager.getEtudiantByTag(uid);
        cours = coursManager.getCoursEnCours();




        String heureDebutCours = cours.getHeureDebut();
        SimpleDateFormat heureFormat = new SimpleDateFormat("HH:mm");
        Date heureCours = new Date();
        try {
            heureCours = heureFormat.parse(heureDebutCours);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar heureRetard = Calendar.getInstance();
        heureRetard.setTime(heureCours);
        heureRetard.add(Calendar.MINUTE, 10);


        Date heureArriveeEtudiant = new Date();
        String heureEtudiant = heureFormat.format(heureArriveeEtudiant);

        Date dateEtudiant = new Date();
        try {
            dateEtudiant = heureFormat.parse(heureEtudiant);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar heureEtudiantCompare = Calendar.getInstance();
        heureEtudiantCompare.setTime(dateEtudiant);


        if(etudiant.getNumeroEtudiant() > 0){

            if(heureRetard.after(heureEtudiantCompare)){
                statutEtudiant = "present";
            } else {
                statutEtudiant = "en retard";
            }
            presence = new Presence(etudiant.getNumeroEtudiant(),cours.getCodeCours(),cours.getDateCours(),statutEtudiant,heureEtudiant);
            presenceManager.addPresence(presence);

            //modifier presence grâce au tag et fermer l'activity
            Intent recuperationIdTag = new Intent(LectureTag.this, PresenceEtudiant.class);
            recuperationIdTag.putExtra("idTag", uid);
            startActivity(recuperationIdTag);
            LectureTag.this.finish();
        } else {
            Intent recuperationIdTag = new Intent(LectureTag.this, CreationEtudiantTag.class);
            recuperationIdTag.putExtra("idTag", uid);
            startActivity(recuperationIdTag);
            LectureTag.this.finish();
        }
    }
}
