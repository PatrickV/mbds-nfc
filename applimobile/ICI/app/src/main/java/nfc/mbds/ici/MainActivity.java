package nfc.mbds.ici;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    private Button newEtudiant = null;
    private Button commencerCours = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newEtudiant = (Button) findViewById(R.id.creerEtudiant);
        commencerCours = (Button) findViewById(R.id.debutCours);

        newEtudiant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent creationEtudiantActivite = new Intent(MainActivity.this, CreationEtudiant.class);

                startActivity(creationEtudiantActivite);
            }
        });

        commencerCours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent debutCoursActivite = new Intent(MainActivity.this, DebutCours.class);
                startActivity(debutCoursActivite);
            }
        });




    }


}
