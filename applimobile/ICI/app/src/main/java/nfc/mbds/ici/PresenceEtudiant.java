package nfc.mbds.ici;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.CoursManager;
import nfc.mbds.ici.bdd.EtudiantManager;
import nfc.mbds.ici.bdd.PresenceManager;

public class PresenceEtudiant extends Activity {
    private Button btnTerminer = null;
    private TextView txtNFC = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presence);

        txtNFC = (TextView) findViewById(R.id.NFC);

        final EtudiantManager etudiantManager = new EtudiantManager(this);
        final CoursManager coursManager = new CoursManager(this);
        final PresenceManager presenceManager = new PresenceManager(this);

        etudiantManager.open();
        etudiantManager.getEtudiants();
        etudiantManager.close();

        coursManager.open();
        presenceManager.open();

        Cours coursEnCours = coursManager.getCoursEnCours();

        btnTerminer = (Button) findViewById(R.id.bTerminer);
        int nbEtudiantPresent = presenceManager.getNombreEtudiantPresent(coursEnCours.getCodeCours());
        txtNFC.setText("Etudiant présents : " + nbEtudiantPresent);

        if (nbEtudiantPresent > 0){
            btnTerminer.setEnabled(true);
        } else {
            btnTerminer.setEnabled(false);
        }

        btnTerminer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent creationEtudiantActivite = new Intent(PresenceEtudiant.this, FinCours.class);

                startActivity(creationEtudiantActivite);
            }
        });
    }



}
