package nfc.mbds.ici.bdd;


public class Cours {
    private int codeCours;
    private String nomCours;
    private int idProfesseur;
    private  String dateCours;
    private String heureDebut;
    private String coursFini;

    public Cours(int codeCours, String nomCours, int idProfesseur, String dateCours, String heureDebut, String coursFini){
        this.codeCours = codeCours;
        this.nomCours = nomCours;
        this.idProfesseur = idProfesseur;
        this.dateCours = dateCours;
        this.heureDebut = heureDebut;
        this.coursFini = coursFini;
    }

    public int getCodeCours() {
        return codeCours;
    }

    public void setCodeCours(int codeCours) {
        this.codeCours = codeCours;
    }

    public String getNomCours() {
        return nomCours;
    }

    public void setNomCours(String nomCours) {
        this.nomCours = nomCours;
    }

    public int getIdProfesseur() {
        return idProfesseur;
    }

    public void setIdProfesseur(int idProfesseur) {
        this.idProfesseur = idProfesseur;
    }

    public String getDateCours() {
        return dateCours;
    }

    public void setDateCours(String dateCours) {
        this.dateCours = dateCours;
    }

    public String getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

    public String getCoursFini() {
        return coursFini;
    }

    public void setCoursFini(String coursFini) {
        this.coursFini = coursFini;
    }
}
