package nfc.mbds.ici.bdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CoursManager {
    private static final String TABLE_NAME = "Cours";
    public static final String KEY_CODE_COURS = "CodeCours";
    public static final String KEY_NOM_COURS = "NomCours";
    public static final String KEY_ID_PROFESSEUR = "NomProfesseur";
    public static final String KEY_DATE_COURS = "DateCours";
    public static final String KEY_HEURE_DEBUT = "HeureDebut";
    public static final String KEY_COURS_FINI = "CoursFini";
    public static final String CREATE_TABLE_COURS = "CREATE TABLE " + TABLE_NAME + " ("
            + KEY_CODE_COURS + " INTEGER primary key, "
            + KEY_NOM_COURS + " TEXT, "
            + KEY_ID_PROFESSEUR + " INTEGER, "
            + KEY_DATE_COURS + " TEXT, "
            + KEY_HEURE_DEBUT + " TEXT, "
            + KEY_COURS_FINI + " TEXT );";

    private MySQLite baseDeDonnees;
    private SQLiteDatabase bd;

    public CoursManager(Context context){
        baseDeDonnees = MySQLite.getInstance(context);
    }

    public void open(){
        bd = baseDeDonnees.getWritableDatabase();
    }

    public void close(){
        bd.close();
    }

    public long addCours(Cours cours){
        ContentValues values = new ContentValues();

        values.put(KEY_CODE_COURS, cours.getCodeCours());
        values.put(KEY_NOM_COURS, cours.getNomCours());
        values.put(KEY_ID_PROFESSEUR, cours.getIdProfesseur());
        values.put(KEY_DATE_COURS, cours.getDateCours());
        values.put(KEY_HEURE_DEBUT, cours.getHeureDebut());
        values.put(KEY_COURS_FINI, cours.getCoursFini());

        return bd.insert(TABLE_NAME, null,values);
    }

    public int modCours(Cours cours){
        ContentValues values = new ContentValues();

        values.put(KEY_NOM_COURS, cours.getNomCours());
        values.put(KEY_ID_PROFESSEUR, cours.getIdProfesseur());
        values.put(KEY_DATE_COURS, cours.getDateCours());
        values.put(KEY_HEURE_DEBUT, cours.getHeureDebut());
        values.put(KEY_COURS_FINI, cours.getCoursFini());

        String where = KEY_CODE_COURS + " = ?";
        String[] whereArgs = {cours.getCodeCours() + ""};

        return bd.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supCours(Cours cours){
        String where = KEY_CODE_COURS+ " = ?";
        String[] whereArgs = {cours.getCodeCours() + ""};

        return bd.delete(TABLE_NAME, where, whereArgs);
    }

    public Cours getCours(String codeCours){
        Cours cours = new Cours(0,"",0,"", "", "");

        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME +" WHERE " + KEY_CODE_COURS + " = " + codeCours, null);
        if (c.moveToFirst()){
            cours.setCodeCours(c.getInt((c.getColumnIndex(KEY_CODE_COURS))));
            cours.setNomCours(c.getString(c.getColumnIndex(KEY_NOM_COURS)));
            cours.setIdProfesseur(c.getInt(c.getColumnIndex(KEY_ID_PROFESSEUR)));
            cours.setDateCours(c.getString(c.getColumnIndex(KEY_DATE_COURS)));
            cours.setHeureDebut(c.getString(c.getColumnIndex(KEY_HEURE_DEBUT)));
            cours.setCoursFini(c.getString(c.getColumnIndex(KEY_COURS_FINI)));
        }
        return cours;
    }

    public Cours getCoursByNom(String nomCours){
        Cours cours = new Cours(0,"",0,"","","");

        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_NOM_COURS + " = \"" + nomCours + "\"", null);
        if (c.moveToFirst()){
            cours.setCodeCours(c.getInt((c.getColumnIndex(KEY_CODE_COURS))));
            cours.setNomCours(c.getString(c.getColumnIndex(KEY_NOM_COURS)));
            cours.setIdProfesseur(c.getInt(c.getColumnIndex(KEY_ID_PROFESSEUR)));
            cours.setDateCours(c.getString(c.getColumnIndex(KEY_DATE_COURS)));
            cours.setHeureDebut(c.getString(c.getColumnIndex(KEY_HEURE_DEBUT)));
            cours.setCoursFini(c.getString(c.getColumnIndex(KEY_COURS_FINI)));
        }
        return cours;
    }

    public Cours getCoursEnCours(){
        Cours cours = new Cours(0,"",0,"","","");

        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COURS_FINI + " = \"enCours\"", null);
        if (c.moveToFirst()){
            cours.setCodeCours(c.getInt((c.getColumnIndex(KEY_CODE_COURS))));
            cours.setNomCours(c.getString(c.getColumnIndex(KEY_NOM_COURS)));
            cours.setIdProfesseur(c.getInt(c.getColumnIndex(KEY_ID_PROFESSEUR)));
            cours.setDateCours(c.getString(c.getColumnIndex(KEY_DATE_COURS)));
            cours.setHeureDebut(c.getString(c.getColumnIndex(KEY_HEURE_DEBUT)));
            cours.setCoursFini(c.getString(c.getColumnIndex(KEY_COURS_FINI)));
        }
        return cours;
    }

    public List<String> getListCoursJour(){
        List<String> listeCours = new ArrayList<>();
        Date dateDuJour = new Date();
        SimpleDateFormat formatDateDuJour = new SimpleDateFormat("dd-MM-yyyy");
        String dateJour = formatDateDuJour.format(dateDuJour);
        Cursor c = bd.rawQuery("SELECT " + KEY_NOM_COURS + " FROM " + TABLE_NAME + " WHERE " + KEY_DATE_COURS + " = \"" + dateJour + "\" AND " + KEY_COURS_FINI + " = \"pasCommmence\"" , null);

        if(c.moveToFirst()){
            do {
                listeCours.add(c.getString(0));
            } while (c.moveToNext());
        }
        c.close();
        return listeCours;
    }
}
