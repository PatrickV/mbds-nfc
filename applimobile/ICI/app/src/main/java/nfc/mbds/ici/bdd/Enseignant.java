package nfc.mbds.ici.bdd;

public class Enseignant {
    private int idEnseignant;
    private String nomEnseignant;
    private String codeEnseignant;

    public Enseignant (int idEnseignant, String nomEnseignant, String codeEnseignant){
        this.idEnseignant = idEnseignant;
        this.nomEnseignant = nomEnseignant;
        this.codeEnseignant = codeEnseignant;
    }

    public int getIdEnseignant() {
        return idEnseignant;
    }

    public void setIdEnseignant(int idProfesseur) {
        this.idEnseignant = idProfesseur;
    }

    public String getNomEnseignant() {
        return nomEnseignant;
    }

    public void setNomEnseignant(String nomEnseignant) {
        this.nomEnseignant = nomEnseignant;
    }

    public String getCodeEnseignant() {
        return codeEnseignant;
    }

    public void setCodeEnseignant(String codeEnseignant) {
        this.codeEnseignant = codeEnseignant;
    }
}
