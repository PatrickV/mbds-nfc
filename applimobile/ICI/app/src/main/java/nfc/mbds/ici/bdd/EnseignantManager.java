package nfc.mbds.ici.bdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class EnseignantManager {
    private static final String TABLE_NAME = "Enseignant";
    public static final String KEY_ID_ENSEIGNANT = "IdEnseignant";
    public static final String KEY_NOM_ENSEIGNANT = "NomEnseignant";
    public static final String KEY_CODE_ENSEIGNANT = "CodeEnseignant";
    public static final String CREATE_TABLE_ENSEIGNANT = "CREATE TABLE " + TABLE_NAME + " ("
            + KEY_ID_ENSEIGNANT + " INT primary key,"
            + KEY_NOM_ENSEIGNANT + " TEXT, "
            + KEY_CODE_ENSEIGNANT + " TEXT);";

    private MySQLite baseDeDonnnes;
    private SQLiteDatabase bd;

    public EnseignantManager(Context context) {
        baseDeDonnnes = MySQLite.getInstance(context);
    }

    public void open(){
        bd = baseDeDonnnes.getWritableDatabase();
    }

    public void close(){
        bd.close();
    }

    public long addEnseignant (Enseignant enseignant){
        ContentValues values = new ContentValues();
        values.put(KEY_ID_ENSEIGNANT, enseignant.getIdEnseignant());
        values.put(KEY_NOM_ENSEIGNANT, enseignant.getNomEnseignant());
        values.put(KEY_CODE_ENSEIGNANT, enseignant.getCodeEnseignant());

        return bd.insert(TABLE_NAME,null, values);
    }

    public int modEnseignant (Enseignant enseignant){
        ContentValues values = new ContentValues();
        values.put(KEY_NOM_ENSEIGNANT, enseignant.getNomEnseignant());
        values.put(KEY_CODE_ENSEIGNANT, enseignant.getCodeEnseignant());

        String where = KEY_ID_ENSEIGNANT + " = ?";
        String[] whereArgs = {enseignant.getIdEnseignant() + ""};
        return bd.update(TABLE_NAME, values, where, whereArgs);
    }

    public Enseignant getEnseignant(int idEnseignant){
        Enseignant enseignant = new Enseignant(0,"", "");

        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID_ENSEIGNANT + " = " +  idEnseignant, null);
        if(c.moveToFirst()){
            enseignant.setIdEnseignant(c.getInt(c.getColumnIndex(KEY_ID_ENSEIGNANT)));
            enseignant.setNomEnseignant(c.getString(c.getColumnIndex(KEY_NOM_ENSEIGNANT)));
            enseignant.setCodeEnseignant(c.getString(c.getColumnIndex(KEY_CODE_ENSEIGNANT)));
        }
        return enseignant;
    }


    public List getEnseignants(){
        List<Enseignant> listeEnseignant = new ArrayList<>();
        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        if(c.moveToFirst()){
            do {
                Enseignant enseignant = new Enseignant(c.getInt(0), c.getString(1), c.getString(2));
                listeEnseignant.add(enseignant);
            } while (c.moveToNext());
        }
        c.close();
        return listeEnseignant;
    }
}
