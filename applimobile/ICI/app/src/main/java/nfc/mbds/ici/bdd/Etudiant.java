package nfc.mbds.ici.bdd;

public class Etudiant {
    private int numeroEtudiant;
    private String nomEtudiant;
    private String prenomEtudiant;
    private String password;
    private String tagNFC;

    public Etudiant (int numero_etudiant, String nom_etudiant, String prenom_etudiant, String password, String tag_nfc){
        this.numeroEtudiant = numero_etudiant;
        this.nomEtudiant = nom_etudiant;
        this.prenomEtudiant = prenom_etudiant;
        this.password = password;
        this.tagNFC = tag_nfc;
    }

    public int getNumeroEtudiant(){
        return numeroEtudiant;
    }

    public void setNumeroEtudiant(int numero_etudiant){
        this.numeroEtudiant = numero_etudiant;
    }

    public String getNom_etudiant() {
        return nomEtudiant;
    }

    public void setNom_etudiant(String nom_etudiant) {
        this.nomEtudiant = nom_etudiant;
    }

    public String getPrenom_etudiant() {
        return prenomEtudiant;
    }

    public void setPrenom_etudiant(String prenom_etudiant) {
        this.prenomEtudiant = prenom_etudiant;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTag_nfc() {
        return tagNFC;
    }

    public void setTag_nfc(String tag_nfc) {
        this.tagNFC = tag_nfc;
    }
}
