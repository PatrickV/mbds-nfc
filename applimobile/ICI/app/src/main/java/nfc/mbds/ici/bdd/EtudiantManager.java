package nfc.mbds.ici.bdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EtudiantManager {
    private static final String TABLE_NAME = "Etudiant";
    public static final String KEY_NUMERO_ETUDIANT = "NumeroEtudiant";
    public static final String KEY_NOM_ETUDIANT = "NomEtudiant";
    public static final String KEY_PRENOM_ETUDIANT = "PrenomEtudiant";
    public static final String KEY_PASSWORD = "Password";
    public static final String KEY_TAG_NFC = "Tag";
    public static final String CREATE_TABLE_ETUDIANT = "CREATE TABLE " + TABLE_NAME + " ("
            + KEY_NUMERO_ETUDIANT + " INTEGER primary key, "
            + KEY_NOM_ETUDIANT + " TEXT, "
            + KEY_PRENOM_ETUDIANT + " TEXT, "
            + KEY_PASSWORD + " TEXT, "
            + KEY_TAG_NFC + " TEXT ); ";

    private MySQLite baseDeDonnees;
    private SQLiteDatabase bd;

    public EtudiantManager(Context context){
        baseDeDonnees = MySQLite.getInstance(context);
    }

    public void open(){
        bd = baseDeDonnees.getWritableDatabase();
    }

    public void close() {
        bd.close();
    }

    public long addEtudiant (Etudiant etudiant){
        ContentValues values = new ContentValues();

        values.put(KEY_PRENOM_ETUDIANT, etudiant.getPrenom_etudiant());
        values.put(KEY_TAG_NFC, etudiant.getTag_nfc());
        values.put(KEY_NUMERO_ETUDIANT, etudiant.getNumeroEtudiant());
        values.put(KEY_NOM_ETUDIANT, etudiant.getNom_etudiant());
        values.put(KEY_PASSWORD, etudiant.getPassword());

        return bd.insert(TABLE_NAME, null, values);
    }

    public int modEtudiant (Etudiant etudiant){
        ContentValues values = new ContentValues();
        values.put(KEY_NOM_ETUDIANT, etudiant.getNom_etudiant());
        values.put(KEY_PRENOM_ETUDIANT, etudiant.getPrenom_etudiant());
        values.put(KEY_PASSWORD, etudiant.getPassword());
        values.put(KEY_TAG_NFC, etudiant.getTag_nfc());

        String where = KEY_NUMERO_ETUDIANT + " = ?";
        String[] whereArgs = {etudiant.getNumeroEtudiant() + ""};

        return bd.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supEtudiant(Etudiant etudiant){
        String where = KEY_NUMERO_ETUDIANT+ " = ?";
        String[] whereArgs = {etudiant.getNumeroEtudiant() + ""};

        return bd.delete(TABLE_NAME, where, whereArgs);
    }

    public Etudiant getEtudiant(int numeroEtudiant){
        Etudiant etudiant = new Etudiant(0, "", "", "", "");

        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_NUMERO_ETUDIANT + "=" + numeroEtudiant, null);
        if (c.moveToFirst()){
            etudiant.setNumeroEtudiant(c.getInt(c.getColumnIndex(KEY_NUMERO_ETUDIANT)));
            etudiant.setNom_etudiant(c.getString(c.getColumnIndex(KEY_NOM_ETUDIANT)));
            etudiant.setPrenom_etudiant(c.getString(c.getColumnIndex(KEY_PRENOM_ETUDIANT)));
            etudiant.setPassword((c.getString(c.getColumnIndex(KEY_PASSWORD))));
            etudiant.setTag_nfc(c.getString(c.getColumnIndex(KEY_TAG_NFC)));
        }
        return etudiant;
    }

    public int getEtudiantSansTag() {
        Etudiant etudiant = new Etudiant(0, "", "", "", "");
        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_TAG_NFC + " IS NULL AND " + KEY_NUMERO_ETUDIANT + "!= 10000000", null);
        if(c.moveToFirst()){
            etudiant.setNumeroEtudiant(c.getInt(c.getColumnIndex(KEY_NUMERO_ETUDIANT)));
            etudiant.setNom_etudiant(c.getString(c.getColumnIndex(KEY_NOM_ETUDIANT)));
            etudiant.setPrenom_etudiant(c.getString(c.getColumnIndex(KEY_PRENOM_ETUDIANT)));
            etudiant.setPassword((c.getString(c.getColumnIndex(KEY_PASSWORD))));
            etudiant.setTag_nfc(c.getString(c.getColumnIndex(KEY_TAG_NFC)));
        }
        return etudiant.getNumeroEtudiant();
    }

    public Etudiant getEtudiantByTag(String tag){
        Etudiant etudiant = new Etudiant(0,"","","","");
        Cursor c;
        try {
            c =bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_TAG_NFC + " = \"" + tag + "\"" , null);
            if(c == null){
                return etudiant;
            } else {
                if (c.moveToFirst()) {
                    etudiant.setNumeroEtudiant(c.getInt(c.getColumnIndex(KEY_NUMERO_ETUDIANT)));
                    etudiant.setNom_etudiant(c.getString(c.getColumnIndex(KEY_NOM_ETUDIANT)));
                    etudiant.setPrenom_etudiant(c.getString(c.getColumnIndex(KEY_PRENOM_ETUDIANT)));
                    etudiant.setPassword((c.getString(c.getColumnIndex(KEY_PASSWORD))));
                    etudiant.setTag_nfc(c.getString(c.getColumnIndex(KEY_TAG_NFC)));
                }
            }
            return  etudiant;
        } catch(Exception e) {
            return etudiant;
        }
    }

    public Cursor getEtudiants(){
        return bd.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }
}
