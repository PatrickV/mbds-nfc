package nfc.mbds.ici.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import nfc.mbds.ici.PresenceEtudiant;

public class MySQLite extends SQLiteOpenHelper {
    private static final String DATABASE_NAME  = "db.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static MySQLite sInstance;

    public static synchronized MySQLite getInstance(Context context){
        if (sInstance == null){
            sInstance = new MySQLite(context);
        }
        return sInstance;
    }

    public MySQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EtudiantManager.CREATE_TABLE_ETUDIANT);
        db.execSQL(CoursManager.CREATE_TABLE_COURS);
        db.execSQL(EnseignantManager.CREATE_TABLE_ENSEIGNANT);
        db.execSQL(PresenceManager.CREATE_TABLE_PRESENCE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onCreate(db);
    }
}
