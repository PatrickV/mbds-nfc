package nfc.mbds.ici.bdd;

public class Presence {
    private int numero_etudiant;
    private int id_cours;
    private String date_cours;
    private String statut;
    private String heure_arrivee;

    public Presence (int numeroEtudiant, int codeCours, String dateCours, String statutEtudiant, String heureDebutCours){
        this.numero_etudiant = numeroEtudiant;
        this.id_cours = codeCours;
        this.date_cours = dateCours;
        this.statut = statutEtudiant;
        this.heure_arrivee = heureDebutCours;

    }

    public int getNumeroEtudiant(){
        return numero_etudiant;
    }

    public void setNumeroEtudiant(int numeroEtudiant) {
        this.numero_etudiant = numeroEtudiant;
    }

    public int getCodeCours() {
        return id_cours;
    }

    public void setCodeCours(int codeCours) {
        this.id_cours = codeCours;
    }

    public String getHeureDebutCours() {
        return heure_arrivee;
    }

    public void setHeureDebutCours(String heureDebutCours) {
        this.heure_arrivee = heureDebutCours;
    }

    public String getDateCours() {
        return date_cours;
    }

    public void setDateCours(String dateCours) {
        this.date_cours = dateCours;
    }

    public String getStatutEtudiant() {
        return statut;
    }

    public void setStatutEtudiant(String statutEtudiant) {
        this.statut = statutEtudiant;
    }
}
