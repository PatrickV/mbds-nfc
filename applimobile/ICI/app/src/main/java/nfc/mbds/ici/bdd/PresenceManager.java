package nfc.mbds.ici.bdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class PresenceManager {
    private static final String TABLE_NAME = "Presence";
    public static final String KEY_NUMERO_ETUDIANT = "NumeroEtudiant";
    public static final String KEY_CODE_COURS = "CodeCours";
    public static final String KEY_DATE_COURS = "DateCours";
    public static final String KEY_HEURE_DEBUT_COURS = "HeureDebutCours";
    public static final String KEY_STATUT_ETUDIANT = "statutEtudiant";
    public static final String CREATE_TABLE_PRESENCE = " CREATE TABLE " + TABLE_NAME + " ("
            + KEY_NUMERO_ETUDIANT + " INTEGER, "
            + KEY_CODE_COURS + " INTEGER, "
            + KEY_DATE_COURS + " TEXT, "
            + KEY_HEURE_DEBUT_COURS + " TEXT,"
            + KEY_STATUT_ETUDIANT + " TEXT,"
            + " PRIMARY KEY (" + KEY_NUMERO_ETUDIANT + ", "
            + KEY_CODE_COURS + ", "
            + KEY_DATE_COURS + "));";

    private MySQLite baseDeDonnees;
    private SQLiteDatabase bd;

    public PresenceManager(Context context){
        baseDeDonnees = MySQLite.getInstance(context);
    }

    public void open(){
        bd = baseDeDonnees.getWritableDatabase();
    }

    public void close(){
        bd.close();
    }

    public long addPresence(Presence presence){
        ContentValues values = new ContentValues();

        values.put(KEY_NUMERO_ETUDIANT, presence.getNumeroEtudiant());
        values.put(KEY_CODE_COURS, presence.getCodeCours());
        values.put(KEY_DATE_COURS, presence.getDateCours());
        values.put(KEY_HEURE_DEBUT_COURS, presence.getHeureDebutCours());
        values.put(KEY_STATUT_ETUDIANT, presence.getStatutEtudiant());

        return bd.insert(TABLE_NAME, null, values);
    }

    public int modPresence(Presence presence){
        ContentValues values = new ContentValues();
        values.put(KEY_STATUT_ETUDIANT, presence.getStatutEtudiant());

        String where = KEY_NUMERO_ETUDIANT + "=? AND " + KEY_CODE_COURS + "=? AND " + KEY_DATE_COURS + "=? AND " + KEY_HEURE_DEBUT_COURS + "=?";
        String[] whereArgs = {presence.getNumeroEtudiant() + "", presence.getCodeCours() + "", presence.getDateCours() + "", presence.getDateCours() + ""};

        return bd.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supPresence(Presence presence){
        String where = KEY_NUMERO_ETUDIANT + "=? AND " + KEY_CODE_COURS + "=? AND " + KEY_DATE_COURS + "=? AND " + KEY_HEURE_DEBUT_COURS + "=?" ;
        String[] whereArgs = {presence.getNumeroEtudiant() + "", presence.getCodeCours() + "", presence.getDateCours() + "", presence.getDateCours() + ""};

        return bd.delete(TABLE_NAME, where, whereArgs);
    }

    public int getNombreEtudiantPresent(int idCours){
        Cursor cursor = bd.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_CODE_COURS + " = " + idCours,null);
        int nombre = cursor.getCount();

        return nombre;
    }

    public Cursor getPresences(int numCours){
        return bd.rawQuery("SELECT Presence." + KEY_NUMERO_ETUDIANT + " as _id, Etudiant.PrenomEtudiant, Etudiant.NomEtudiant," + KEY_STATUT_ETUDIANT + " FROM " + TABLE_NAME + ", Etudiant WHERE " + KEY_CODE_COURS + " = " + numCours + " AND Presence.NumeroEtudiant = Etudiant.NumeroEtudiant", null);
    }

    public List<Object> getListePresence(int numCours){
        List<Object> listePresence = new ArrayList<>();
        Cursor c = bd.rawQuery("SELECT * FROM " + TABLE_NAME + ", Etudiant WHERE " + KEY_CODE_COURS + " = " + numCours, null);

        if(c.moveToFirst()){
            do {
                Presence presence = new Presence(c.getInt(0),c.getInt(1), c.getString(2), c.getString(4), c.getString(3));
                listePresence.add(presence);
            } while (c.moveToNext());
        }
        c.close();
        return listePresence;
    }
}
