package nfc.mbds.ici.communicationApi;


import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.Enseignant;
import nfc.mbds.ici.bdd.Etudiant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Api {
    private static Api instance;
    private Retrofit retrofit;
    private WebService service;
    private Context context;
    private SharedPreferences sharedPreferences;

    public static Api getInstance(Context context){
        if(instance == null){
            instance = new Api(context);
        }

        return instance;
    }

    public Api(Context context){
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.43.254:3000")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(WebService.class);
        sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
    }

    public void getListeCours(Callback callback) {
        Call<List<Cours>> call = service.listCours();
        call.enqueue(callback);
    }

    public void getListeEnseignant(Callback callback){
        Call<List<Enseignant>> call = service.listeEnseignant();
        call.enqueue(callback);
    }

    public void envoieListePresence(List<Object> listePresence, Callback callback){
        Call call = service.envoiePresence(listePresence);
        call.enqueue(callback);
    }

    public void envoieNewEtudiant(Etudiant newEtudiant, Callback callback){
        Call call = service.envoieEtudiant(newEtudiant);
        call.enqueue(callback);
    }

}