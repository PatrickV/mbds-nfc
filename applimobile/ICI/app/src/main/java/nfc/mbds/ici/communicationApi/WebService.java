package nfc.mbds.ici.communicationApi;

import java.util.List;

import nfc.mbds.ici.bdd.Cours;
import nfc.mbds.ici.bdd.Enseignant;
import nfc.mbds.ici.bdd.Etudiant;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WebService {
    @GET("api/cours")
    Call<List<Cours>> listCours();

    @GET("api/enseignants")
    Call<List<Enseignant>> listeEnseignant();

    @POST("api/presence")
    Call<ReponseVide> envoiePresence(@Body List<Object> listePresence);

    @POST("api/etudiants")
    Call<ReponseVide> envoieEtudiant(@Body Etudiant ajoutEtudiant);

}
