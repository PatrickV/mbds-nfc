**Prérequis :**  
- nodejs  
- mariaDB ou MySQL  

**Installation :**  
- executer le script sql (serveurweb/database.sql) pour créer la base  
- ouvrir un terminal de commande dans serveurweb puis executer `npm install`  
- modifier la configuration d'accès à la base dans serveurweb/config/database.js  

**Exécution :**  
- ouvrir un terminal dans serveurweb puis lancer le serveur avec `node bin/www`  
- se connecter sur [localhost:3000](http://localhost:3000) (le login par défault est : admin@admin.fr et le mot de passe est : admin )  

**Informations complémentaires :**  
- Par souci de simplicité pour l'ajout de données, les mot de passe ne sont pas cryptés, pour une mise en production, il faudrait crypter les mots de passes avec un algorithme tel que Bcrypt.

**Avant lancement de l'application mobile**
- Pour assurer la connection entre le webservice et l'application web il faut faire la modification suivant :
- Dans le fichier Api se trouvant dans le dossier communicationApi, il faut modifier l'URL de l'application web