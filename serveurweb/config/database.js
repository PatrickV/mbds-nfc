var mysql = require('mysql');

var config = {
    // domaine name or ip address of the database
    host: '127.0.0.1',
    // User of the database
    user: 'root',
    // Password of the user
    password: 'toor',
    // Name of the database
    database: 'nfc'
}

var connection = mysql.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database
});

module.exports = connection; 