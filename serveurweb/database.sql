CREATE DATABASE  IF NOT EXISTS `nfc` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `nfc`;
-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: nfc
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administration`
--

DROP TABLE IF EXISTS `administration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `administration` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` text COLLATE utf8_unicode_ci NOT NULL,
  `prenom` text COLLATE utf8_unicode_ci NOT NULL,
  `mot_de_passe` text COLLATE utf8_unicode_ci NOT NULL,
  `adresse_email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_email` (`adresse_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administration`
--

LOCK TABLES `administration` WRITE;
/*!40000 ALTER TABLE `administration` DISABLE KEYS */;
INSERT INTO `administration` VALUES (1,'admin','admin','admin','admin@admin.fr');
/*!40000 ALTER TABLE `administration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cours`
--

DROP TABLE IF EXISTS `cours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cours` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_matiere` int(11) unsigned NOT NULL,
  `salle` text COLLATE utf8_unicode_ci NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `a_commence` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cours`
--

LOCK TABLES `cours` WRITE;
/*!40000 ALTER TABLE `cours` DISABLE KEYS */;
INSERT INTO `cours` VALUES (1,1,'MBDS','2019-01-22 17:00:00','2019-01-22 20:00:00',0),(2,2,'308','2019-01-30 16:00:00','2019-01-30 19:00:00',0);
/*!40000 ALTER TABLE `cours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `cours_information`
--

DROP TABLE IF EXISTS `cours_information`;
/*!50001 DROP VIEW IF EXISTS `cours_information`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `cours_information` AS SELECT 
 1 AS `intitule`,
 1 AS `nom`,
 1 AS `prenom`,
 1 AS `salle`,
 1 AS `date_debut`,
 1 AS `date_fin`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `enseignant`
--

DROP TABLE IF EXISTS `enseignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `enseignant` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` text COLLATE utf8_unicode_ci NOT NULL,
  `prenom` text COLLATE utf8_unicode_ci NOT NULL,
  `code` text COLLATE utf8_unicode_ci NOT NULL,
  `mot_de_passe` text COLLATE utf8_unicode_ci NOT NULL,
  `adresse_email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `enseignant_email` (`adresse_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enseignant`
--

LOCK TABLES `enseignant` WRITE;
/*!40000 ALTER TABLE `enseignant` DISABLE KEYS */;
INSERT INTO `enseignant` VALUES (1,'Dupont','Pierre','1234','1234','pierre.dupont@unice.fr'),(2,'LaFleure','Michelle','5678','5678','michelle.lafleure@unice.fr');
/*!40000 ALTER TABLE `enseignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `etudiant` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` text COLLATE utf8_unicode_ci NOT NULL,
  `prenom` text COLLATE utf8_unicode_ci NOT NULL,
  `numero_etudiant` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `mot_de_passe` text COLLATE utf8_unicode_ci NOT NULL,
  `adresse_email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `etudiant_email` (`adresse_email`),
  UNIQUE KEY `unique_etudiant_numero_etudiant` (`numero_etudiant`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etudiant`
--

LOCK TABLES `etudiant` WRITE;
/*!40000 ALTER TABLE `etudiant` DISABLE KEYS */;
INSERT INTO `etudiant` VALUES (1,'Vincent-Cuaz','Laure','21404504','test','LaureVincent-Cuaz@etu.unice.fr');
/*!40000 ALTER TABLE `etudiant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `login_informations`
--

DROP TABLE IF EXISTS `login_informations`;
/*!50001 DROP VIEW IF EXISTS `login_informations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `login_informations` AS SELECT 
 1 AS `adresse_email`,
 1 AS `mot_de_passe`,
 1 AS `categorie`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `matiere` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_enseignant` int(11) unsigned NOT NULL,
  `intitule` text COLLATE utf8_unicode_ci NOT NULL,
  `id_administration` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `matiere_enseignant` (`id_enseignant`),
  KEY `matiere_administration` (`id_administration`),
  CONSTRAINT `matiere_administration` FOREIGN KEY (`id_administration`) REFERENCES `administration` (`id`),
  CONSTRAINT `matiere_enseignant` FOREIGN KEY (`id_enseignant`) REFERENCES `enseignant` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matiere`
--

LOCK TABLES `matiere` WRITE;
/*!40000 ALTER TABLE `matiere` DISABLE KEYS */;
INSERT INTO `matiere` VALUES (1,1,'Maths',1),(2,2,'Anglais',1);
/*!40000 ALTER TABLE `matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `matiere_information`
--

DROP TABLE IF EXISTS `matiere_information`;
/*!50001 DROP VIEW IF EXISTS `matiere_information`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `matiere_information` AS SELECT 
 1 AS `ens_nom`,
 1 AS `ens_prenom`,
 1 AS `intitule`,
 1 AS `adm_nom`,
 1 AS `adm_prenom`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `presence`
--

DROP TABLE IF EXISTS `presence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `presence` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero_etudiant` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id_cours` int(11) unsigned NOT NULL,
  `heure_arrivee` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `presence_cours` (`id_cours`),
  KEY `presence_numero_etudiant` (`numero_etudiant`),
  CONSTRAINT `presence_cours` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id`),
  CONSTRAINT `presence_numero_etudiant` FOREIGN KEY (`numero_etudiant`) REFERENCES `etudiant` (`numero_etudiant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presence`
--

LOCK TABLES `presence` WRITE;
/*!40000 ALTER TABLE `presence` DISABLE KEYS */;
/*!40000 ALTER TABLE `presence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `presence_information`
--

DROP TABLE IF EXISTS `presence_information`;
/*!50001 DROP VIEW IF EXISTS `presence_information`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `presence_information` AS SELECT 
 1 AS `intitule`,
 1 AS `status`,
 1 AS `heure_arrivee`,
 1 AS `date_debut`,
 1 AS `numero_etudiant`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `cours_information`
--

/*!50001 DROP VIEW IF EXISTS `cours_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cours_information` AS select `m`.`intitule` AS `intitule`,`e`.`nom` AS `nom`,`e`.`prenom` AS `prenom`,`c`.`salle` AS `salle`,`c`.`date_debut` AS `date_debut`,`c`.`date_fin` AS `date_fin` from ((`matiere` `m` join `enseignant` `e`) join `cours` `c`) where `c`.`id_matiere` = `m`.`id` and `m`.`id_enseignant` = `e`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `login_informations`
--

/*!50001 DROP VIEW IF EXISTS `login_informations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `login_informations` AS select `a`.`adresse_email` AS `adresse_email`,`a`.`mot_de_passe` AS `mot_de_passe`,'administration' AS `categorie` from `administration` `a` union select `etu`.`adresse_email` AS `adresse_email`,`etu`.`mot_de_passe` AS `mot_de_passe`,'etudiant' AS `categorie` from `etudiant` `etu` union select `ens`.`adresse_email` AS `adresse_email`,`ens`.`mot_de_passe` AS `mot_de_passe`,'enseingnant' AS `categorie` from `enseignant` `ens` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `matiere_information`
--

/*!50001 DROP VIEW IF EXISTS `matiere_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `matiere_information` AS select `e`.`nom` AS `ens_nom`,`e`.`prenom` AS `ens_prenom`,`m`.`intitule` AS `intitule`,`a`.`nom` AS `adm_nom`,`a`.`prenom` AS `adm_prenom` from ((`enseignant` `e` join `matiere` `m`) join `administration` `a`) where `e`.`id` = `m`.`id_enseignant` and `a`.`id` = `m`.`id_administration` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `presence_information`
--

/*!50001 DROP VIEW IF EXISTS `presence_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `presence_information` AS select `m`.`intitule` AS `intitule`,`p`.`status` AS `status`,`p`.`heure_arrivee` AS `heure_arrivee`,`c`.`date_debut` AS `date_debut`,`p`.`numero_etudiant` AS `numero_etudiant` from ((`presence` `p` join `cours` `c`) join `matiere` `m`) where `p`.`id_cours` = `c`.`id` and `c`.`id_matiere` = `m`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-22 20:54:27
