document.addEventListener('DOMContentLoaded', function() {
   
    if (document.getElementById('search-student')) {
        document.getElementById('search-student').onclick = function () {
            let number = document.getElementById('student-number-to-find').value;

            if (number.length > 0) {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', '/administration/absences/' + number);
                xhr.onload = function () {
                    if (xhr.status === 200) {
                        let absences = JSON.parse(xhr.responseText);
 
                        let html = "";
                        absences.forEach((absence) => {
                            html += "<tr><td>" + absence.intitule + "</td><td>" + absence.status + "</td><td>" + absence.heure_arrivee + "</td><td>" + absence.date_debut + "</td></tr>";
                        });

                        let table = document.getElementById('insert-data-here');
                        table.innerHTML = html;
                        document.getElementById('hidden-table').removeAttribute('hidden');
                    } else if (xhr.status === 404) {
                        let html = "Cet étudiant n'a pas d'absence !";
                        let div = document.getElementById('no-absence');
                        div.innerHTML = html;
                    } else {
                        let container = document.getElementById('to-display-error');
                        container.removeAttribute('hidden');
                    }
                };
                xhr.send();
            }
        }
    }
});
