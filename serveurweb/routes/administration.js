var express = require('express');
var db = require('../config/database');

var router = express.Router();

router.use(function (req, res, next) {
    res.locals.user = req.session.user;
    res.locals.group = req.session.category;
    next();
});

router.use(function (req, res, next) {
    if (req.session.user != undefined) {
        if (req.session.category !== 'administration') {
            switch (req.session.category) {
                case 'enseignant':
                    res.redirect('/enseignants');
                    break;

                case 'etudiant':
                    res.redirect('/etudiants');
                    break;

                default:
                    res.redirect('/');
                    break;
            }
        }
        next();
    } else {
        res.redirect('/login');
    }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('administration/index', {title: 'Statut des étudiants'});
});

router.get('/admin/liste', function (req, res, next) {
    db.query('select nom, prenom, adresse_email from administration', function (err, result) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
            res.render('error');
        }
        res.render('administration/admin/liste', {title: 'Les des administrateurs', liste: result });
    });
    
});
router.get('/admin/ajouter', function (req, res, next) {
    res.render('administration/admin/ajouter', {title: 'Ajouter un administrateur'});
});

router.post('/admin/ajouter', function (req, res, next) {
    let firstname = req.body['admin-firstname'];
    let lastname = req.body['admin-lastname'];
    let email = req.body['admin-email'];
    let password = req.body['admin-password'];

    db.query('insert into administration(nom, prenom, mot_de_passe, adresse_email) values ?', [[[lastname, firstname, password, email]]], function (err, rows) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
            return res.render('error');
        }
        res.redirect('/administration/admin/liste');
    });
});

router.get('/cours/liste', function (req, res, next) {
    db.query('select * from cours_information', function (err, result) {
		if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			res.render('error');
        }
        for (i in result) {
            let date_debut = new Date(result[i].date_debut);
            let date_fin = new Date(result[i].date_fin);

            let date_debut_hour = (date_debut.getHours() < 10) ? '0' + date_debut.getHours() : date_debut.getHours();
            let date_debut_minutes = (date_debut.getMinutes() < 10) ? '0' + date_debut.getMinutes() : date_debut.getMinutes();
            let date_debut_day = (date_debut.getDate() < 10) ? '0' + date_debut.getDate() : date_debut.getDate();
            let date_debut_month = ((date_debut.getMonth() + 1) < 10) ? '0' + (date_debut.getMonth() + 1) : (date_debut.getMonth() + 1);

            result[i].date_debut = date_debut_hour + ':' + date_debut_minutes + ' ' + date_debut_day + '-' + date_debut_month + '-' + date_debut.getFullYear();

            let date_fin_hour = (date_fin.getHours() < 10) ? '0' + date_fin.getHours() : date_fin.getHours();
            let date_fin_minutes = (date_fin.getMinutes() < 10) ? '0' + date_fin.getMinutes() : date_fin.getMinutes();
            let date_fin_day = (date_fin.getDate() < 10) ? '0' + date_fin.getDate() : date_fin.getDate();
            let date_fin_month = ((date_fin.getMonth() + 1) < 10) ? '0' + (date_fin.getMonth() + 1) : (date_fin.getMonth() + 1);

            result[i].date_fin = date_fin_hour + ':' + date_fin_minutes + ' ' + date_fin_day + '-' + date_fin_month + '-' + date_fin.getFullYear();
        }

		res.render('administration/cours/liste', {title: 'Liste des cours', liste: result});
	});
});

router.get('/cours/ajouter', function (req, res, next) {
    db.query('select id, intitule from matiere', function (err, result) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
            res.render('error');
        }
        res.render('administration/cours/ajouter', {title: 'Ajouter un cours', matieres: result});
    });
});

router.post('/cours/ajouter', function (req, res, next) {
    let dateRegexp = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/

    let matiere = req.body['course-name'];
    let salle = req.body['course-room'];
    let debut = req.body['course-start'];
    let fin = req.body['course-end'];

    if (!debut.match(dateRegexp)) {        
        res.locals.debut = true;
        return res.redirect('/administration/cours/ajouter');
    }

    if (!fin.match(dateRegexp)) {
        res.locals.fin = true;
        return res.redirect('/administration/cours/ajouter');
    }

    db.query('insert into cours(id_matiere, salle, date_debut, date_fin, a_commence) values ?', [[[parseInt(matiere), salle, debut, fin, 0]]], function (err, rows) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			return res.render('error');
        }
        res.redirect('/administration/cours/liste');
    });    
});

router.get('/matieres/liste', function (req, res, next) {
    db.query('select ens_nom, ens_prenom, intitule, adm_nom, adm_prenom from matiere_information', function (err, result) {
		if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			res.render('error');
        }        
		res.render('administration/matieres/liste', {title: 'Liste des matières', liste: result});
	});
});


router.get('/matieres/ajouter', function (req, res, next) {
    db.query('select id, nom, prenom from enseignant', function (err, result_ens) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
            res.render('error');
        }
        db.query('select id, nom, prenom from administration;', function (err, result_adm) {
            if (err) {
                res.locals.message = err.message;
                res.locals.error = err;
                res.render('error');
            }
            res.render('administration/matieres/ajouter', { title: 'Ajouter une matière', liste_ens: result_ens, liste_adm: result_adm });
        });
    });
});

router.post('/matieres/ajouter', function (req, res, next) {
    let teacher = req.body['course-teacher'];
    let name = req.body['course-name'];
    let admin = req.body['course-admin'];

    db.query('insert into matiere(id_enseignant, intitule, id_administration) values ?', [[[teacher, name, admin]]], function (err, rows) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			return res.render('error');
        }
        res.redirect('/administration/matieres/liste');
    });
});

router.get('/enseignants/liste', function (req, res, next) {
    db.query('select nom, prenom, code, adresse_email from enseignant', function (err, result) {
		if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			res.render('error');
        }        
		res.render('administration/enseignants/liste', {title: 'Liste des enseignants', liste: result});
	});
});

router.get('/enseignants/ajouter', function (req, res, next) {
    res.render('administration/enseignants/ajouter', {title: 'Ajouter un enseignant'});
});

router.post('/enseignants/ajouter', function (req, res, next) {
    let firstname = req.body['teacher-firstname'];
    let lastname = req.body['teacher-lastname'];
    let code = req.body['teacher-code'];
    let email = req.body['teacher-email'];
    let password = req.body['teacher-password'];

    db.query('insert into enseignant(nom, prenom, code, mot_de_passe, adresse_email) values ?', [[[lastname, firstname, code, password, email]]], function (err, rows) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			return res.render('error');
        }
        res.redirect('/administration/enseignants/liste');
    });
});

router.get('/etudiants/liste', function (req, res, next) {
    db.query('select nom, prenom, numero_etudiant, adresse_email from etudiant', function (err, result) {
		if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			res.render('error');
        }        

		res.render('administration/etudiants/liste', {title: 'Liste des etudiants', liste: result});
	});
});

router.get('/etudiants/ajouter', function (req, res, next) {
    res.render('administration/etudiants/ajouter', {title: 'Ajouter un etudiant'});
});

router.post('/etudiants/ajouter', function (req, res, next) {

    let firstname = req.body['student-firstname'];
    let lastname = req.body['student-lastname'];
    let number = req.body['student-number'];
    let password = req.body['student-password'];
    let email = req.body['student-email'];

    db.query('insert into etudiant(nom, prenom, numero_etudiant, mot_de_passe, adresse_email) values ?', [[[lastname, firstname, number, password, email]]], function (err, rows) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
			return res.render('error');
        }
        res.redirect('/administration/etudiants/liste');
    });
});

router.get('/absences/:student_number', function (req, res, next) {
    db.query("select intitule, status, heure_arrivee, date_debut from presence_information where numero_etudiant = ?", req.params.student_number, function (err, result) {
        if (err) {
            res.locals.message = err.message;
            res.locals.error = err;
            res.sendStatus(500);
        }

        if (result.length > 0) {
            return res.send(result);
        } else {
            return res.sendStatus(404);
        }
    });
});

module.exports = router;
