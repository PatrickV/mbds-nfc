var express = require('express');
var db = require('../config/database');

var router = express.Router();

router.get('/cours', function (req, res, next) {
	let date = new Date();
	let start = dateCreatorStart(date);
	let end = dateCreatorEnd(date);

	db.query('select * from cours where date_debut > ? and date_fin < ?', [start, end], function (err, result) {
		if (err) {
			return res.send({error: err, data: ""});
		}
		if (result.length > 0) {
			return res.send({error: "", data: result});
		} else {
			return res.status(404).send('No course found');
		}
	});
});

router.get('/enseignants', function (req, res, next) {
	db.query('select nom, prenom, code from enseignant', function (err, result) {
		if (err) {
			return res.send({error: err, data: ""});
		}
		if (result.length > 0) {
			return res.send({error: "", data: result});
		} else {
			return res.status(404).send('No teacher found');
		}
	});
});

router.get('/etudiants', function (req, res, next) {
	db.query('select nom, prenom, numero_etudiant from etudiant', function (err, result) {
		if (err) {
			return res.send({error: err, data: ""});
		}
		if (result.length > 0) {
			return res.send({error: "", data: result});
		} else {
			return res.status(404).send('No student found');
		}
	});
});

router.post('/etudiants', function (req, res, next) {
	if (JSON.stringify(req.body).length > 2) {
		console.log(req.body);
		let student = req.body;
		let values = [];
		values.push([student.nomEtudiant, student.prenomEtudiant, student.numeroEtudiant, student.password, student.prenomEtudiant + student.nomEtudiant + "@etu.unice.fr"]);

		db.query('insert into etudiant(nom, prenom, numero_etudiant, mot_de_passe, adresse_email) values?', [values])
			.on('error', function (err) {
				return res.status(424).send(err);
			})
			.on('result', function (rows) {
				return res.status(201).send('""');
			});
	} else {
		return res.status(200).send('""');
	}
});

router.post('/presence', function (req, res, next) {
	console.log(req.body);
	if (req.body.length > 0) {
		
		let values = [];
		req.body.forEach(row => {
			values.push([row.numero_etudiant, row.id_cours, row.heure_arrivee + ' ' + row.date_cours, row.statut]);
		});

		console.log(values);
		

		db.query('insert into presence(numero_etudiant, id_cours, heure_arrivee, status) values?', [values], function (err, result) {
			if (err) {
				console.log("coucou je suis une erreur");
				
				return res.send({ error: err});
			}
			return res.status(201).send('""');
		});
	} else {
		return res.status(200).send('""');
	}

	// id_cours
	// num etu
	// date cour
	// heure arrivée
	// status
});

module.exports = router;

function dateCreatorStart(inputDate) {
	let d = (inputDate.getDate() < 10) ? '0' + inputDate.getDate() : inputDate.getDate();
	let m = ((inputDate.getMonth() + 1) < 10) ? '0' + (inputDate.getMonth() + 1) : inputDate.getMonth() + 1;
	let y = inputDate.getFullYear();

	return y + '-' + m + '-' + d + ' 00:00:00';
}

function dateCreatorEnd(inputDate) {
	let d = (inputDate.getDate() < 10) ? '0' + inputDate.getDate() : inputDate.getDate();
	let m = ((inputDate.getMonth() + 1) < 10) ? '0' + (inputDate.getMonth() + 1) : inputDate.getMonth() + 1;
	let y = inputDate.getFullYear();

	return y + '-' + m + '-' + d + ' 23:59:59';
}