var express = require('express');

var router = express.Router();

router.use(function (req, res, next) {
    res.locals.user = req.session.user;
    res.locals.group = req.session.category;
    next();
});

router.use(function (req, res, next) {
    if (req.session.user != undefined) {
        if (req.session.category !== 'enseignant') {
            switch (req.session.category) {
                case 'administration':
                    res.redirect('/administration');
                    break;

                case 'etudiant':
                    res.redirect('/etudiants');
                    break;

                default:
                    res.redirect('/');
                    break;
            }
        }
        next();
    } else {
        res.redirect('/login');
    }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('enseignants/index', {title: 'Enseignants'});
});

router.get('/cours', function (req, res, next) {
    res.render('enseignants/cours', {title: 'Mes cours'});
});

router.get('/absences', function (req, res, next) {
    res.render('enseignants/absences', {title: 'Les absences'});
});

module.exports = router;