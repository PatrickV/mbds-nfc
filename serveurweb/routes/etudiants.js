var express = require('express');

var router = express.Router();

router.use(function (req, res, next) {
    res.locals.user = req.session.user;
    res.locals.group = req.session.category;
    next();
});

router.use(function (req, res, next) {
    if (req.session.user != undefined) {
        if (req.session.category !== 'etudiant') {
            switch (req.session.category) {
                case 'administration':
                    res.redirect('/administration');
                    break;
                
                case 'enseignant':
                    res.redirect('/enseignants');
                    break;
                
                default:
                    res.redirect('/');
                    break;
            }
        }
        next();
    } else {
        res.redirect('/login');
    }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('etudiants/index', {title: 'Etudiants'});
});

router.get('/cours', function (req, res, next) {
    res.render('etudiants/cours', {title: 'Mes cours'});
});

router.get('/absences', function (req, res, next) {
    res.render('etudiants/absences', {title: 'Mes absences'});
});

module.exports = router;
