var express = require('express');
var db = require('../config/database');

var router = express.Router();

router.use(function (req, res, next) {
  res.locals.user = req.session.user;
  res.locals.group = req.session.category;
  next();
});

/* GET home page. */
router.get('/', function (req, res, next) {
  if (req.session.category !== undefined) {
    switch (req.session.category) {
      case 'etudiant':
        res.redirect('/etudiants');
        break;

      case 'enseignant':
        res.redirect('/enseignants');
        break;

      case 'administration':
        res.redirect('/administration');
        break;
    }
  }
  res.redirect('/login');
});

router.get('/login', function (req, res, next) {
  if (req.session.category !== undefined) {
    switch (req.session.category) {
      case 'etudiant':
        res.redirect('/etudiants');
        break;
      
      case 'enseignant':
        res.redirect('/enseignants');
        break;
      
      case 'administration':
        res.redirect('/administration');
        break;
    }
  }
  res.render('login', {title: 'Login'});
});

router.post('/login', function (req, res, next) {
  email = req.body.userEmail;
  password = req.body.userPassword;
  
  db.query('select * from login_informations where adresse_email = ? and mot_de_passe = ?', [email, password])
  .on('error', function (err) {
    res.locals.message = err.message;
    res.locals.error = err;
    res.redirect('/login');
  })
  .on('result', function (row) {
    req.session.user = row;
    req.session.category = row.categorie;
    
    switch (row.categorie) {
      case 'etudiant':
        res.redirect('/etudiants');
        break;
      
      case 'enseignant':
        res.redirect('/enseignants');
        break;
      
      case 'administration':
        res.redirect('/administration');
        break;
      
      default:
        res.redirect('/');
        break;
    }
  });
});

router.get('/logout', function (req, res, next) {
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;
